#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <ctime>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    srand(time(NULL));
    ui->setupUi(this);
    srand(time(NULL));
    connect(ui->pushButton,SIGNAL(clicked(bool)),this,SLOT(buttonPressed()));

    serv = new QTcpServer(this);
    connect(serv,SIGNAL(newConnection()),this,SLOT(newConnection()));

    sock = new QTcpSocket(this);
    mrTimer = new QTimer(this);

    connCount = 0;
    x_coord = 100;
    sgn = 1;

    connect(mrTimer,SIGNAL(timeout()),this,SLOT(timeout()));

    if (serv->listen(QHostAddress::Any,8080))
    //if (serv->listen(QHostAddress("169.254.79.86"),7))
        ui->label_6->setText("Server started");
    else
        ui->label_6->setText("Network error");

   //mrTimer->start(1000);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::timeout()
{
    if (x_coord == 200)
        sgn = -1;
    if (x_coord == 100)
        sgn = 1;
    x_coord += sgn*10;

    //Первая команда
//    QString coords = "2 1 ";
//    coords.append(QString::number(x_coord));
//    coords.append(".1 100.25 0.785 2 200.0 200.34 0.56");
//    //Вторая команда
//    coords.append(" 2 1 315.02 98.89 0.03 2 20.45 ");
//    coords.append(QString::number(x_coord)+50);
//    coords.append(".28 1.15\r\n");

    //Первая команда
    QString coords = "0 ";
    //Вторая команда
    if (rand()%2 == 0)
    {
        coords.append("2 3 315 98 5 20 ");
        coords.append(QString::number(x_coord+50));
    }
    else
    {
        coords.append("3 1 315 98 11 20 ");
        coords.append(QString::number(x_coord+50));
        coords.append(" 7 110 80");
    }


    coords.append("\r\n");

    qDebug() << coords;

    ui->label->setText(coords);
    if (sock->isOpen())
        sock->write(coords.toUtf8());
}

void MainWindow::newConnection()
{
    sock = serv->nextPendingConnection();
    connect(sock,SIGNAL(readyRead()),this,SLOT(readSock()));
    connect(sock,SIGNAL(disconnected()),this,SLOT(disconnected()));
    ui->label_6->setText(QString("New connection ").append(connCount++));
    mrTimer->start(1000);
    //sock->write("hello",5);
}

void MainWindow::disconnected()
{
    ui->label_6->setText("Socket disconnected");
    mrTimer->stop();
}

void MainWindow::readSock()
{
    QString str = sock->readLine();
   // sock->write(str);
    ui->label_4->setText(str);
//    qDebug()<<"New data: " << str.size() <<" bytes";
//    qDebug()<<str;
}

void MainWindow::buttonPressed()
{
    if (sock->isOpen())
    {
        QString str = "rand: ";
        str.append(QString::number(rand()%10));
        str.append("\r\n");
        sock->write(str.toUtf8());
        ui->label->setText(str);
    }
}
