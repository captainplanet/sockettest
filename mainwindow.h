#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpSocket>
#include <QTcpServer>
#include <QTimer>
#include <ctime>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QTcpServer* serv;
    QTcpSocket* sock;
    QTimer* mrTimer;
    int connCount;
    int x_coord;
    int sgn;

public slots:
    void buttonPressed();
    void newConnection();
    void disconnected();
    void readSock();

    void timeout();
};

#endif // MAINWINDOW_H
